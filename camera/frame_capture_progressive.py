import numpy as np
import os
import cv2
import time

def print_fn():
    print("")
    print(" 0 0 ")
    print("  |  ")
    print(" .-. ")
    print("")

i = 0

runtime = input("enter the runtime in seconds : ") 
if runtime.isdigit():
    runtime = int(runtime)
else: 
    runtime = 10
    print('the images will be captured for 10 seconds')

try:
    cap = cv2.VideoCapture(0)

except Exception:
    print("error video capture")

try:
    print("connecting to the camera .........")
#    cap.open("rtsp://admin:Quant1ph1@59.152.53.203:554/Streaming/channels/1/")
    cap.open("rtsp://admin:Quant1ph1@192.168.4.120:554/Streaming/channels/1/")
    if(not(cap.isOpened())):
        raise Exception
#    print("connection stablished ")

except Exception as e:
    print("Error connecting to the camera due to following error:  ")
    print(e)

else:
    try :
        timeout = time.time() + runtime
        print("timeout set")
        while True:
            if(not(cap.isOpened())):
                print("connection closed")
                break

            tNamec = str(time.asctime(time.gmtime())) 

            ret, frame = cap.read()
            print("frame captured")
            w, h = frame.shape[:2]
            print("hight and width of imgage is : ",w,h)

        
            tNameb = str(time.asctime(time.gmtime()))
            if (w > 0 and h > 0):
                cv2.imwrite(tNameb+"_"+str(i)+".jpg",frame)
                print("frame saved")

            else :
                print("inalid image")

        
            tNamea = str(time.asctime(time.gmtime()))
            if tNameb == tNamea == tNamec :
                print(i)
            else:
                i=0
            i += 1
    
            if time.time() > timeout:
                print("Oops!! timeout occured")
                print_fn()
                cap.release()
                break
        
    except KeyboardInterrupt :
        print("Oops!! Keyboard Interruption occure ")
        cap.release()
    
    except Exception as e : 
        print('error as below :')
        print(e)
        cap.release()
    
finally :
    print("cloasing camera connection")
    cap.release()
    print("camera connection closed")
